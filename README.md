# SunLightFramework

[![CI Status](http://img.shields.io/travis/egoropra/SunLightFramework.svg?style=flat)](https://travis-ci.org/egoropra/SunLightFramework)
[![Version](https://img.shields.io/cocoapods/v/SunLightFramework.svg?style=flat)](http://cocoapods.org/pods/SunLightFramework)
[![License](https://img.shields.io/cocoapods/l/SunLightFramework.svg?style=flat)](http://cocoapods.org/pods/SunLightFramework)
[![Platform](https://img.shields.io/cocoapods/p/SunLightFramework.svg?style=flat)](http://cocoapods.org/pods/SunLightFramework)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SunLightFramework is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SunLightFramework"
```

## Author

egoropra, pr45opra@gmail.com

## License

SunLightFramework is available under the MIT license. See the LICENSE file for more info.
