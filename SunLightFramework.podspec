#
# Be sure to run `pod lib lint SunLightFramework.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SunLightFramework'
  s.version          = '1.0'
  s.summary          = 'A short description of SunLightFramework.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/rot_ream/sunlightframework'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'egoropra' => 'pr45opra@gmail.com' }
  s.source           = { :git => 'https://pr45opra@bitbucket.org/rot_ream/sunlightframework.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'SunLightFramework/Classes/**/*'
  
  s.resource_bundles = {
    'SunLightFramework' => ['SunLightFramework/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Firebase/Core'
    s.dependency 'Firebase/AdMob'
   # s.dependency 'VungleSDK-iOS'
   # s.dependency 'OneSignal'
    s.dependency 'EZLoadingActivity'
    s.dependency 'Alamofire'
    s.dependency 'SwiftyJSON'
   # s.dependency 'JSQActivityKit'
   # s.dependency 'AdColony'
    s.dependency 'SwiftyStoreKit'
    s.dependency 'SDWebImage'
end
