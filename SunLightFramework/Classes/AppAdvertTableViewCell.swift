//
//  AppAdvertTableViewCell.swift
//  SunLightFramework
//
//  Created by Egor on 24.07.16.
//  Copyright © 2016 rot. All rights reserved.
//

import UIKit

class AppAdvertTableViewCell: UITableViewCell {

    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var labelAppName: UILabel!
    @IBOutlet weak var imageStars: UIImageView!
    @IBOutlet weak var labelReviewCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imageIcon.layer.cornerRadius = 20
        imageIcon.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
