//
//  AppAdvertManager.swift
//  SunLightFramework
//
//  Created by Egor on 25.07.16.
//  Copyright © 2016 rot. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON


class AppAdvertManager {
    

    class func start()
    {
        
//        let manager: Manager = {
//            let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
//            configuration.requestCachePolicy = .ReloadIgnoringLocalCacheData
//            return Manager(configuration: configuration)
//        }()
        
        Alamofire.request(.GET, "http://myrot.ru/app/get_data.php")
            .responseJSON{ response in
                
                if let js = response.result.value {
                    
                    let json = JSON(js)
                    var appAdvertArray = [AppAdvert]()
                    
                    for appElement in json.arrayObject! {
                        
                        
                        let appAdvert = AppAdvert()
                        appAdvert.name = appElement["name"] as! String
                        appAdvert.link = appElement["link"] as! String
                        appAdvert.imageUrl = appElement["img_url"] as! String
                        
                        appAdvert.reviewCount = SunLightManager.randomInt(100,max: 1500)
                        
                        appAdvert.starCount =  SunLightManager.randomInt(4,max: 5)
                        
                         let bundleID = NSBundle.mainBundle().bundleIdentifier
                        
                        appAdvertArray.append(appAdvert)
  
                    }
                    
                    let appAdVc = AppAdvertViewController(nibName: "AppAdvertViewController", bundle: NSBundle(forClass: SunLightFramework.self))
                    appAdVc.appAdvertArray = appAdvertArray
                    
                    let window =   UIApplication.sharedApplication().delegate?.window
                    window!!.rootViewController?.presentViewController(appAdVc, animated: true, completion: {
                        
                    })
                    
                }
                
                
        }
    }
    
}


