//
//  ShuherViewController.swift
//  SunLightFramework
//
//  Created by Egor on 25.07.16.
//  Copyright © 2016 rot. All rights reserved.
//

import UIKit

class ShuherViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let url = NSURL (string: "http://myrot.ru/shuher.html");
        let requestObj = NSURLRequest(URL: url!);
        self.webView.loadRequest(requestObj);

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
