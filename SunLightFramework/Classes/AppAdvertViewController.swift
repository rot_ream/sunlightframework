//
//  AppAdvertViewController.swift
//  SunLightFramework
//
//  Created by Egor on 24.07.16.
//  Copyright © 2016 rot. All rights reserved.
//

import UIKit
import SDWebImage

class AppAdvertViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate{
  
    @IBOutlet weak var viewPlatform: UIView!
   
    @IBOutlet weak var imageViewPlatform: UIImageView!

    @IBOutlet weak var tableView: UITableView!
    
    
    var appAdvertArray = [AppAdvert]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // self.tableView.registerClass(AppAdvertTableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
        self.tableView.registerNib(UINib(nibName: "AppAdvertTableViewCell", bundle: NSBundle(forClass: SunLightFramework.self)), forCellReuseIdentifier: "cell")
        
        UIView.animateWithDuration(0.5, delay: 0.0,
                                   options: [.Repeat,  .Autoreverse], animations: {
                                    self.imageViewPlatform.transform = CGAffineTransformMakeScale(0.7, 0.7)
            },
                                   completion: { finish in
                                    UIView.animateWithDuration(0.5){
                                        self.imageViewPlatform.transform = CGAffineTransformIdentity
                                    }
        })

        
        let tap = UITapGestureRecognizer(target: self, action: #selector(AppAdvertViewController.handleTap(_:)))
        tap.delegate = self
        viewPlatform.addGestureRecognizer(tap)
        
        
    
        
    }
    
    
    @IBAction func buttonCloseTouchUpInside(sender: AnyObject) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
       UIApplication.sharedApplication().openURL(NSURL(string: "http://chestnii-zarabotok.ru/")!)
    }
    
    

    override func viewWillAppear(animated: Bool)
    {
        //First Call Super
        super.viewWillAppear(animated)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
 
    

    // MARK: - Table view data source
    
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return appAdvertArray.count
    }
    
    
    
     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
     {
        return 112
        //testik
     }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       // let lastRowIndex = tableView.numberOfRowsInSection(tableView.numberOfSections-1)
        
//        if (indexPath.row == lastRowIndex - 1) {
//            print("last row selected")
//            
//            let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
//           // cell.textLabel?.text = "Close"
//            return cell
//        }
//        
        let appAdvert = appAdvertArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! AppAdvertTableViewCell
        
        cell.imageIcon.sd_setImageWithURL(NSURL(string: appAdvert.imageUrl)!)
        
        cell.labelAppName.text = appAdvert.name
        
        cell.labelReviewCount.text =  "(" + String(appAdvert.reviewCount) + ")"
        
        
//        if(appAdvert.starCount == 4)
//        {
//            cell.imageStars.image = UIImage(named:"stars_4")
//            
//        } else if(appAdvert.starCount == 5)
//        {
//            cell.imageStars.image = UIImage(named:"stars_5")
//        }
        

        return cell
    }
    
    

    
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        // UIApplication.sharedApplication().openURL(NSURL(string: )
        
           UIApplication.sharedApplication().openURL(NSURL(string: appAdvertArray[indexPath.row].link)!)
        
        
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */


}
