//
//  PreferencesManager.swift
//  SunLightNew
//


import Foundation


class PreferencesManager {
    
    
    enum MyError: ErrorType {
        case KeyEmptyError
        case InvalidKeyError
    }
    
    
    let appKey : String
    
    let searchBarHidden : Bool
    
    var smallBannerKey : String
    var fullScreenBannerKey : String
    let nativeBannerKey : String
    
    let webViewUrl : String
    let customStyleUrl : String
    
    let customStyleString : String
    
    let javaScriptAction : String
    
    let defaultColors : Bool
    
    let statusBarHidden : Bool
    
    var isSimulator : Bool = false
    
    var isDisableAdvert : Bool = false
    
    var inAppPurchase : Bool = false
    
    let inAppPurchaseSKU : String
    
    
    var fullScreenBannerInterval : Int = 60
    
    //Мусорные ссылки, по которым зарпещаем редиректы
    var trashUrls : [String]
    //Мусорные ссылки, по которым выводим алерт
    var trashButtonsUrls : [String]
    
    
    class var sharedInstance: PreferencesManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: PreferencesManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            
                do {
                     Static.instance = try PreferencesManager()
                }
                catch {
                    print("Error: \(error)")
            }
 
        }
        
        return Static.instance!
    }
    
    
    func disableAdvert() {
        
        isDisableAdvert = true
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(true, forKey: "isDisableAdvert")
        defaults.synchronize()
        
    }
    
    private init() throws {
        
        
        let defaults = NSUserDefaults.standardUserDefaults()
        self.isDisableAdvert = defaults.boolForKey("isDisableAdvert")
        
        let rootKey = "ZZ__ROT__ZZ"
        
        //        do {
        //             self.searchBarHidden = try? (NSBundle.mainBundle().infoDictionary![rootKey]?["SearchBarHidden"] as? Bool)!
        //        } catch {
        //            self.searchBarHidden = true
        //        }
        //
        
        if let searchBarHidden = NSBundle.mainBundle().infoDictionary?[rootKey]?["SearchBarHidden"] as? Bool {
            self.searchBarHidden = searchBarHidden
        }
        else
        {
            self.searchBarHidden = true
        }
        
        if let appKey = NSBundle.mainBundle().infoDictionary?[rootKey]?["AppKey"] as? String {
            self.appKey = appKey
        }
        else
        {
            self.appKey = ""
        }
        
        
        
        if let smallBannerKey = NSBundle.mainBundle().infoDictionary?[rootKey]?["SmallBannerKey"] as? String {
              self.smallBannerKey = smallBannerKey
        }
        else
        {
            throw NSError(domain: "KeyEmptyError", code: 0, userInfo: ["Key":"SmallBannerKey"])
        }
      
        
        
        
        self.nativeBannerKey = (NSBundle.mainBundle().infoDictionary![rootKey]?["NativeBannerKey"] as? String)!
        
        self.fullScreenBannerKey = (NSBundle.mainBundle().infoDictionary![rootKey]?["FullScreenBannerKey"] as? String)!
        self.webViewUrl = (NSBundle.mainBundle().infoDictionary![rootKey]?["WebViewURL"] as? String)!
        
        self.customStyleUrl = (NSBundle.mainBundle().infoDictionary![rootKey]?["CustomStyleURL"] as? String)!
        self.customStyleString = (NSBundle.mainBundle().infoDictionary![rootKey]?["CustomStyleString"] as? String)!
        self.javaScriptAction = (NSBundle.mainBundle().infoDictionary![rootKey]?["JavaScriptAction"] as? String)!
        
        
        self.defaultColors = (NSBundle.mainBundle().infoDictionary![rootKey]?["DefaultColors"] as? Bool)!
        
        self.inAppPurchase = (NSBundle.mainBundle().infoDictionary![rootKey]?["InAppPurchase"] as? Bool)!
        
        
        self.inAppPurchaseSKU = (NSBundle.mainBundle().infoDictionary![rootKey]?["InAppPurchaseSKU"] as? String)!
        
        self.statusBarHidden = (NSBundle.mainBundle().infoDictionary![rootKey]?["StatusBarHidden"] as? Bool)!
        
        self.trashUrls = (NSBundle.mainBundle().infoDictionary![rootKey]?["TrashUrls"]! as? [String])!
        
        
        if let trashButtonsUrls = NSBundle.mainBundle().infoDictionary![rootKey]?["TrashButtonsUrls"]! as? [String] {
            self.trashButtonsUrls = trashButtonsUrls
        }
        else
        {
            self.trashButtonsUrls = []
        }
        
        
        
        //Регексы для замены
        
        
        // self.trashButtonsUrls = try! (NSBundle.mainBundle().infoDictionary![rootKey]?["TrashButtonsUrls"]! as? [String])!
        
        self.fullScreenBannerInterval = (NSBundle.mainBundle().infoDictionary![rootKey]?["FullScreenBannerInterval"] as? Int)!
        
        
        
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            self.isSimulator = true
        #endif
        
    }
    
    
    
    //получить рекламные обьявы
    
    
}